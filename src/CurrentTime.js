exports.date = ()=>  {
    const today = new Date();
    const year = today.getFullYear();
    const month = today.getMonth() + 1;
    const date = today.getDate();
    let hour = today.getHours();
    const minute = today.getMinutes()<10 ? "0" + today.getMinutes() : today.getMinutes();
    const second = today.getSeconds() <10 ? "0" + today.getSeconds() : today.getSeconds();
    let ampm;
    let day = today.getDay();
    if (day === 0) {
        day = "일요일";
    } else if (day === 1) {
        day = "월요일";
    } else if (day === 2) {
        day = "화요일";
    } else if (day === 3) {
        day = "수요일";
    } else if (day === 4) {
        day = "목요일";
    } else if (day === 5) {
        day = "금요일";
    } else {
        day = "토요일";
    };

    if (hour > 12){
        ampm = "PM"
    }   else {
        ampm = "AM"
    }
    return ({
        year,
        month,
        date,
        day,
        hour,
        minute,
        second,
        ampm}
    );
}