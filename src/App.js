import React, { useEffect, useState } from "react";
import axios from "axios";
import './App.css';
import moment from "moment";
import Moment from "react-moment";
import LoadingOverlay from './component/LoadingOverlay/LoadingOverlay';
import 'react-calendar/dist/Calendar.css';
import "moment/locale/ko";
function App() {
  const [loading, setLoading] = useState(true);
  const [todolist, setTodolist] = useState({ items: []});
  const [open, toggleOpen] = useState(false);
  const [todoCount,setTodoCount] = useState(0); 
  const [edit, setEdit] = useState(false);



  // const [id, setId] = useState("");
  moment.locale('ko');
  // const host= "http://localhost:5000"; //로컬용
  // const host = "https://6867d8cc8266.ngrok.io"; //모바일용
  const host = "https://node-todolist-dfrsvg.cfapps.us10.hana.ondemand.com";
  const handleOpen = (e) =>{
    toggleOpen(!open);

  };

  function handleCloseApp(e) {
    window.location.href = "app://close";
  };

  // function touchStart(e) {
  //   console.log(e.changedTouches[0].clientX)
  //   setStartX(e.changedTouches[0].clientX);
  // };
  // function touchEnd(e) {
  //   const target = document.querySelector(".total-item");
  //   target.style.left = 16;
  //   // target.style.position = "absolute";
  // };
  // function TouchMove(e) {
  //   const id = e.target.id;
  //   const target = document.querySelector(".total-item");
  //   let targetLeft = target.offsetLeft;
  //   targetLeft += (Math.round(e.changedTouches[0].clientX - startX))/100;
    
  //   if (targetLeft > 10){
  //     touchEnd(e);
  //   } else{
  //     target.style.left = targetLeft +"px";
  //     target.style.position = "absolute";
  //   }


  //   // debugger;
  //   // console.log(startX - e.changedTouches[0].clientX);
  // };
  function mouseOver(e) {
    handleContentMouseOver(e);
    handleMouseOver(e);
  };
  function mouseOut(e) {
    handleContentMouseOut(e);
    handleMouseOut(e);
  }
  function handleCancel(e){
    const id = e.currentTarget.id.split("-").reverse()[0];

    // setId(target);
    const copyData = {...todolist};
    copyData.items[id].todo_edit = !copyData.items[id].todo_edit; 
    setTodolist(copyData);
    setEdit(!edit);

  }
  const handleContentMouseOver = (e) => {
    const id = e.currentTarget.id.split("-").reverse()[0];
    const tagId = document.getElementById(`itemlist-${id}`);
    tagId.style.border = "5px solid lightseagreen";

    // debugger;
  };

  const handleContentMouseOut = (e) => {
    const id = e.currentTarget.id.split("-").reverse()[0];
    const tagId = document.getElementById(`itemlist-${id}`);
    tagId.style.border = "";
  }
  function handleMouseOver(e){


    const id = e.currentTarget.id;
    if (id === "circle"){
      document.getElementById(id).style.backgroundColor = "lightseagreen";
    } else if (e.currentTarget.className === "todolist-item-time") {
      
    }
    else{
      document.getElementById(id).style.color = "lightseagreen";
    }
    

  };
  function handleMouseOut(e){
    const id = e.currentTarget.id;
    if ( id === "circle"){
      document.getElementById(id).style.backgroundColor = "seagreen";
    } else if (e.currentTarget.className === "todolist-item-time") {
      
    }else {
      document.getElementById(id).style.color = "dimgray";
    }

  };
  // const handleEditPress = (e) => {
  //   debugger;
  // };
  const handleKeyPressEdit = (e) => {
    if(e.key ==="Enter"){
      setEdit(!edit);
      const todoText = e.target.value;
      const id = e.target.id.split("-").reverse()[0];
      
      const copyData = {...todolist};
      const copyItem = copyData.items[id];


      const item = {
        "id" : copyItem.id,
        "todo_title": todoText,
        "todo_done" : copyItem.todo_done,
        "createAt" : copyItem.createAt,
        "modifiedAt": moment().format("YYYY-MM-DD HH:mm:ss"),
      };
     setLoading(true);
      try {
        sendMessage(copyData.user_id)
        .then((response) =>{
            copyData.items.splice(id,1); //삭제해버리고
            copyData.items.splice(id,0,item)  //다시 push
            
            updateTodolist(copyData).then(response=>{
              if(response.data){
                setTodolist(response.data);
                // setTodoCount(response.data.items.length); 
                e.target.value="";
                toggleOpen(false);
                setLoading(false);
              }
            }).catch(error=>{
              setLoading(false);
          });
        })
        .catch((error)=> {
          setLoading(false);
          // debugger;
        });

      } catch (error) {
        setLoading(false);
        debugger;
      }
 
    };
  };
  const handleKeyPress =(e)=>{

    if(e.key === "Enter"){
      const todoText = e.target.value;
      const copyData = {...todolist};
      // const moment = currDate.date();
      const item = {
        "id": todolist.items.length + 1,
        "todo_title": todoText,
        "todo_done": false,
        "createAt": moment().format("YYYY-MM-DD HH:mm:ss"),
        "modifiedAt": moment().format("YYYY-MM-DD HH:mm:ss"),
      };

      // item.createTime = moment.hour > 12 ? `${moment.year}-${moment.month}-${moment.date} ${moment.ampm} ${moment.hour - 12}:${moment.minute}:${moment.second}` 
      //                                    : `${moment.year}-${moment.month}-${moment.date} ${moment.ampm} ${moment.hour}:${moment.minute}:${moment.second}`;
      setLoading(true);
      try {
        sendMessage(copyData.user_id)
        .then((response) =>{
            copyData.items.push(item);
            delete copyData.items.todo_edit;
            copyData.items.map( (item,idx) => {
              item.id = idx;
              return item;
            });
            updateTodolist(copyData).then(response=>{
              if(response.data){
                setTodolist(response.data);
                setTodoCount(todoCount + 1); 
                e.target.value="";
                toggleOpen(false);
                setLoading(false);
              }
            }).catch(error=>{
              setLoading(false);
          });
        })
        .catch((error)=> {
          setLoading(false);
          // debugger;
        });

      } catch (error) {
        setLoading(false);
        debugger;
      }
      // debugger;
    }
  };

  const sendMessage = (user_id) => {
    const options = {
      url:`${host}/api/v1/todolist/message`,
      method:"POST",
      data:{
        user_id: user_id,
      },
    };
    return axios(options);

  };
  const handleDone= (e) =>{
    // const id = e.target.id.split("-").reverse()[0];
    const id = e.currentTarget.id.split("-").reverse()[0];
    // const copyData = todolist; // shallow copy 주소까지 복사
    // const copyData = JSON.parse(JSON.stringify(todolist)); // deep copy ES%
    const copyData = { ...todolist}; //deepcopy ES6
    setLoading(true);
    
    copyData.items[id].todo_done ? setTodoCount(todoCount+ 1) : setTodoCount(todoCount-1);
    copyData.items[id].todo_done = !copyData.items[id].todo_done;
    copyData.items[id].modifiedAt = moment().format("YYYY-MM-DD HH:mm:ss");
    delete copyData.items.todo_edit;
    updateTodolist(copyData).then( (response) => {
      if(response.data) setTodolist(response.data);
      setLoading(false);
    });
  };
  const handleEdit = (e) => {
    const editConfirm = true;
    // window.confirm("수정하시겠습니까?");
    if (editConfirm === true){
      const id = e.currentTarget.id.split("-").reverse()[0];
      const target = document.getElementById(`text-${id}`);
      // setId(target);
      const copyData = {...todolist};
      copyData.items[id].todo_edit = !copyData.items[id].todo_edit; 
      setTodolist(copyData);
      setEdit(!edit);
      target.focus();

    };
  };

  const handleDelete = (e) =>{
    const deleteConfirm = window.confirm("삭제하시겠습니까?");
    if (deleteConfirm === true){
      const id = e.currentTarget.id.split("-").reverse()[0];
      const copyData = {...todolist};
      setLoading(true);
  

      if (!copyData.items[id].todo_done){
        setTodoCount(todoCount - 1);
      };
      copyData.items.splice(id,1);
      delete copyData.items.todo_edit;
      copyData.items = copyData.items.map( (item,idx) => {
        item.id = idx;
        return item;
      });
      updateTodolist(copyData).then( (response) =>{
        if(response.data) {
          setTodolist(response.data);

        }
        setLoading(false);
      }).catch((error) =>{
        setLoading(false);
        debugger;
      });
    }
  };
  const getTodolist=()=>{
    const options = {
      // url:"http://localhost:5000/api/v1/todolist",
      url:`${host}/api/v1/todolist`,
      method:"GET",
      headers:{
        "user_id" : "2574148",
      }
    };

    return axios(options);
  };
  
  const updateTodolist = (todolist) =>{
    const options = {
      // url:"http://localhost:5000/api/v1/todolist",
      url:`${host}/api/v1/todolist`,
      method:"POST",
      headers:{
        "Content-Type": "application/json",
      },
      data:todolist,
    };
    return axios(options);
  }
  useEffect(() =>{
 
    
    document.getElementsByClassName("todolist-delete")
    setLoading(true);
    getTodolist().then( (response)=>{
      let variable = 0 ;
      if (response.data) {

        response.data.items = response.data.items.map(item => {
          if(!item.todo_done){
            variable = variable + 1;
          }
          item.todo_edit = false;
          return item;
        });
        setTodolist(response.data);
        setTodoCount(variable);
      };
      setLoading(false);    
    }).catch((error)=>{
      setLoading(false);
      // debugger;

    });
    
  },[]);//마지막은 의존성배열 필수
  return(
    <div className="App">
            {/* <h2>{`width is ${width}`}</h2>
      <h2>{`height is ${height}`}</h2> */}

        <div className="app-close" onClick={handleCloseApp}>
          <div style = {{ margin: "10px"}} className="fas fa-times"></div>
        </div>
        {loading && <LoadingOverlay/>}
        <div className ="background">
          <div className ="todolist-date">
            <Moment format='YYYY년 MM월 DD일'>
               {new Date()}
             </Moment>

          </div>
          <div className="todolist-dayofweek">
             <Moment format="dddd">
               {new Date()}
             </Moment>
          </div>
         <div className="todolist-overview">{`할일 ${todoCount}개 남음`}</div>
      </div>
      <div className="todolist-items">
        {todolist.items.map((item, itemIdx) =>{

            return (

              <div id= {`itemlist-${itemIdx}`}className="todolist-item" key={itemIdx} >
              <div id ={`item-content${itemIdx}`} className="todolist-item-content">
                {/* <div className="todolist-check todolist-done"> */}
                <div id={`item-${itemIdx}`} onClick={handleDone}  onMouseOver={mouseOver} onMouseOut={mouseOut}
                        className={item.todo_done ? "todolist-check todolist-done" : "todolist-check"}>
                  {item.todo_done && <i className="fas fa-check"></i>}
                </div>
                {
                item.todo_edit ? 
                                <input autoFocus  id={`text-${itemIdx}`} className={item.todo_done ? "todolist-text text-done" : "todolist-text"}
                                  defaultValue={item.todo_title} onKeyPress={handleKeyPressEdit} onMouseOver={mouseOver} onMouseOut={mouseOut}/>
                               : <div id={`text-${itemIdx}`} onMouseOver={mouseOver} onMouseOut={mouseOut}className={item.todo_done ? "todolist-text text-done" : "todolist-text"}>{ item.todo_title}</div>
                }

                {!item.todo_edit ? 
                <div id={`edit-${itemIdx}`} className="todolist-delete" onMouseOver={mouseOver} onMouseOut={mouseOut} onClick = {handleEdit}>
                  <i className="fas fa-edit" ></i>
                </div>
                :
                <div id={`cancel-${itemIdx}`} className="todolist-cancel" onMouseOver={mouseOver} onMouseOut={mouseOut} onClick = {handleCancel}>
                  <i className="fas fa-times"></i>
                </div>
                }
                <div id={`delete-${itemIdx}`} className="todolist-delete" onMouseOver={mouseOver} onMouseOut={mouseOut} onClick = {handleDelete}>
                  <i className="fas fa-trash"></i>
                </div>
              </div>
              <div id={`timelist-${itemIdx}`} className="todolist-item-time" onMouseOver={mouseOver} onMouseOut={mouseOut}>
                {/* <div className="todolist-Datetime">{`최초생성일 : ${ item.createAt}`}</div> */}
                <div className="todolist-Datetime">{`수정날짜일 : ${ item.modifiedAt}`}</div>
              </div>
              {/* <div className="todolist-Datetime">{hour > 12 ? `${year}-${month}-${date}
                                                   PM ${hour - 12}:${minute}:${second}`
                                                  : `${year}-${currDate.date().month}-${date}
                                                  AM ${hour}:${currDate.date().minute}:${second}`}</div> */}
            </div>
            );
        })}

      </div>
      {open && (
      <div className="bottom-sheet">

          <div className="bottom-input">
            {/* <p>내용 : </p> */}
            <input id="input-content" autoFocus placeholder="할일을 입력 후, Enter을 입력하세요" onKeyPress={handleKeyPress}/>
          </div>

        <div className="bottom-button">
          {/* <button id="button-enter" onClick={handleKeyPress}>등록</button>
          <button onClick={handleOpen}>닫기</button> */}
        </div>
      </div>
  )}
      <div id="circle" className="circle-button" onClick={handleOpen} onMouseOver={handleMouseOver} onMouseOut={handleMouseOut}>
        <i className="fas fa-plus"></i>
      </div>

    </div>
  );
}

export default App;
